
var GRASS = 'G';
var ROAD = 'R';
var CASTLE = 'C';
var NORTH = 0;// north
var EAST = 1;
var SOUTH = 2;
var WEST = 3;
var g; // 2d graphics object on the canvas
var canvas; // the canvas element
var state = NEW_GAME;
var NEW_GAME = 0;
var LOADING = 1;
var RUNNING = 2;
var COMPLETE = 3;
var DEMO = 4;
var cycle = true;
var timer;
var maxcount = 150;
var MW = 5; // Map size in tiles
var MH = MW;
var BW, BH; // Bitmap size in pixels
var SH, SW; // Screen size in tiles
var GW, GH; // Tile size in pixels

var map = new Array(MW * MH);
var cards = new Array(16);

function printType(k) {
    switch (k) {
        case GRASS:
            return '';
        case CASTLE:
            return 'C';
        case ROAD:
            return 'R';
        default:
            return '?';
    }
}

function getMapIdx(x, y) {
    return Math.floor(x + y * MW);
}

function getMap(x, y) {
    if (!inBounds(x, y))
        return 0;
    return map[getMapIdx(x, y)];
}

function getType(x, y, dir) {
    var card = getMap(x, y);
    if (card == null)
        return 0;
    if (card === 0)
        return 0;
    return card.getEdge(dir);
}

function addMap(a) {
    setMap(a.x, a.y, a);
}

function setMap(x, y, a) {
    if (inBounds(x, y)) {
        map[getMapIdx(x, y)] = a;
    }
}

function inBounds(x, y) {
    if (x < 0 || y < 0 || x >= MW || y >= MH) {
        return false;
    }
    return true;
}

//////// GAME CONTROL:

function carcasolo() {
    canvas = document.getElementById('gameCanvas');
    g = canvas.getContext('2d');
    g.imageSmoothingEnabled = false; // dont smooth images (Chrome)
    g.mozImageSmoothingEnabled = false; // dont smooth images (Firefox)

    BW = canvas.width;
    BH = canvas.height;
    SW = MW;
    SH = SW;
    GW = BW / SW;
    GH = BH / SH;

    //Load images
    var i = 0;
    img_x = new Image();
    img_congrat = new Image();
    img_gccc = new Image();
    img_gccg = new Image();
    img_gcgc = new Image();
    img_ggcg = new Image();
    img_ggcr = new Image();
    img_ggrg = new Image();
    img_grcr = new Image();
    img_grrg = new Image();
    img_grrr = new Image();
    img_rccc = new Image();
    img_rccg = new Image();
    img_rccr = new Image();
    img_rgcc = new Image();
    img_rgcg = new Image();
    img_rgrg = new Image();
    img_rrrr = new Image();
    img_x.src = 'http://i.imgur.com/GWSSiTe.png';
    img_congrat.src = 'http://i.imgur.com/HYRhrrV.png';
    img_gccc.src = 'http://i.imgur.com/LPDe0vc.png';
    img_gccg.src = 'http://i.imgur.com/YcmjKfN.png';
    img_gcgc.src = 'http://i.imgur.com/Qi4qa2V.png';
    img_ggcg.src = 'http://i.imgur.com/QXdEMwW.png';
    img_ggcr.src = 'http://i.imgur.com/W4Wzgyt.png';
    img_ggrg.src = 'http://i.imgur.com/XT4Uxji.png';
    img_grcr.src = 'http://i.imgur.com/dhdLSoi.png';
    img_grrg.src = 'http://i.imgur.com/mkh1Ukz.png';
    img_grrr.src = 'http://i.imgur.com/rcoqec7.png';
    img_rccc.src = 'http://i.imgur.com/l23n68r.png';
    img_rccg.src = 'http://i.imgur.com/TEHE4P5.png';
    img_rccr.src = 'http://i.imgur.com/UzIj2z4.png';
    img_rgcc.src = 'http://i.imgur.com/y7yoDEC.png';
    img_rgcg.src = 'http://i.imgur.com/dnSrzE0.png';
    img_rgrg.src = 'http://i.imgur.com/FOHFqNC.png';
    img_rrrr.src = 'http://i.imgur.com/it0Opf9.png';
    cards[i++] = new Card(img_gccc, [GRASS, CASTLE, CASTLE, CASTLE]);
    cards[i++] = new Card(img_gccg, [GRASS, CASTLE, CASTLE, GRASS]);
    cards[i++] = new Card(img_gcgc, [GRASS, CASTLE, GRASS, CASTLE]);
    cards[i++] = new Card(img_ggcg, [GRASS, GRASS, CASTLE, GRASS]);
    cards[i++] = new Card(img_ggcr, [GRASS, GRASS, CASTLE, ROAD]);
    cards[i++] = new Card(img_ggrg, [GRASS, GRASS, ROAD, GRASS]);
    cards[i++] = new Card(img_grcr, [GRASS, ROAD, CASTLE, ROAD]);
    cards[i++] = new Card(img_grrg, [GRASS, ROAD, ROAD, GRASS]);
    cards[i++] = new Card(img_grrr, [GRASS, ROAD, ROAD, ROAD]);
    cards[i++] = new Card(img_rccc, [ROAD, CASTLE, CASTLE, CASTLE]);
    cards[i++] = new Card(img_rccg, [ROAD, CASTLE, CASTLE, GRASS]);
    cards[i++] = new Card(img_rccr, [ROAD, CASTLE, CASTLE, ROAD]);
    cards[i++] = new Card(img_rgcc, [ROAD, GRASS, CASTLE, CASTLE]);
    cards[i++] = new Card(img_rgcg, [ROAD, GRASS, CASTLE, GRASS]);
    cards[i++] = new Card(img_rgrg, [ROAD, GRASS, ROAD, GRASS]);
    cards[i++] = new Card(img_rrrr, [ROAD, ROAD, ROAD, ROAD]);

    btnClick();
}

function setSelectedMapSize() {
    var element = document.querySelector('input[name="sizeRadio"]:checked');
    if(element != null) {
        var sizeStr = element.value;
        MW = parseFloat(sizeStr);
        MH = MW;
        SW = MW;
        SH = SW;
        GW = BW / SW;
        GH = BH / SH;
    }
}

function btnClick() {
    setSelectedMapSize();
    state = LOADING;
    newMap(false);
}

function btn2Click() {
    setSelectedMapSize();
    state = DEMO;
    newMap(true);
}

function newMap(isDemo) {
    map = new Array(MW * MH);
    // make map:
    for (var x = 0; x < MW; x++) {
        for (var y = 0; y < MH; y++) {
            var c;
            var trycount = 0;
            var done = false;
            while (!done) {
                var i = randInt(0, 16);
                c = cards[i].clone(x, y, randInt(0, 4));
                addMap(c);
                trycount ++;
                console.log(x+","+y+" try "+trycount);
                if (c.testv() || trycount >= maxcount) {
                    done = true;
                }
            }
            if (trycount >= maxcount) {
                console.log(x+","+y+" Fit Tile Failed!");
                setMap(x, y, null); // empty tile
            }
        }
    }
    if (!isDemo) {
        // rando flip
        for (var x = 0; x < MW; x++) {
            for (var y = 0; y < MH; y++) {
                var c = getMap(x, y);
                if (c !== null)
                    c.r = randInt(0, 4);
            }
        }
        state = RUNNING;
    }
    draw();
}

function keyDown(event) {

}

function mouseDown(event) {
    event.preventDefault();
}

function mouseUp(event) {
    event.preventDefault();
    if (state === RUNNING) {
        var rect = canvas.getBoundingClientRect();
        var tx = event.clientX - rect.left;
        var ty = event.clientY - rect.top;
        var x = Math.floor(tx / GW);
        var y = Math.floor(ty / GH);
        var c = getMap(x, y);
        if (c != null) {
            c.r--;
            if (c.r < 0)
                c.r = 3;
        }
        draw();
    }
}

//Update and redraw graphics. 
function draw() {
    g.clearRect(0, 0, canvas.width, canvas.height);
    if (state === LOADING) {
        //canvas.SetTextSize(20);
        //canvas.DrawText('Loading', 0.4, 0.3);
    } else if (state === RUNNING || state === COMPLETE || state === DEMO) {
        // Draw Map
        var complete = true;
        for (var i = 0; i < MW; i++) {
            for (var j = 0; j < MH; j++) {
                card = getMap(i, j);
                if (card !== null) {
                    drawRotatedImage(card.img, i * GW, j * GH, GW, GH, (-Math.PI / 2) * card.r);
                    if (!card.test()) {
                        g.drawImage(img_x, i * GW, j * GH, GW, GH);
                        complete = false;
                    }
                }
            }
        }
        if (complete && state === RUNNING) {
            state = COMPLETE;
        }
        if (state === COMPLETE) {
            cycle = !cycle;
            if (cycle) {
                var h = BW*img_congrat.height/img_congrat.width;
                g.drawImage(img_congrat, 10, BH / 2 - h/2, BW, h);
            }
            timer = setTimeout(draw, 200);
        }
    }
}

function drawRotatedImage(image, x, y, w, h, a) { // a is angle in radians
    g.save();
    g.translate(x + w / 2, y + h / 2); // center of rotation
    g.rotate(a);
    g.drawImage(image, -w / 2, -h / 2, w, h);
    g.restore();
}

function randInt(from, to) {
    return from + Math.floor((to - from) * Math.random());
}

// edges is array4 of G,R,C
function Card(img, edges) {
    this.img = img;
    this.edges = edges;
    this.r = 0;
    this.x = 0;
    this.y = 0;
}

Card.prototype.clone = function (x, y, r) {
    var c = new Card(this.img, this.edges);
    c.x = x;
    c.y = y;
    c.r = r;
    return c;
};

Card.prototype.toString = function () {
    return printType(this.edges[0])
            + printType(this.edges[1]) +
            printType(this.edges[2]) +
            printType(this.edges[3]);
};

Card.prototype.getEdge = function (dir) {
    return this.edges[(dir + this.r) % 4];
};

Card.prototype.testv = function () {
    var ne = getType(this.x, this.y - 1, SOUTH);
    var ee = getType(this.x + 1, this.y, WEST);
    var se = getType(this.x, this.y + 1, NORTH);
    var we = getType(this.x - 1, this.y, EAST);
    console.log("nesw:"+ne+","+ee+","+se+","+we);
    console.log("this.nesw:"+this.getEdge(NORTH)+","+this.getEdge(EAST)+","+this.getEdge(SOUTH)+","+this.getEdge(WEST));
    if (ne !== 0 && ne !== this.getEdge(NORTH))
        return false;
    if (ee !== 0 && ee !== this.getEdge(EAST))
        return false;
    if (se !== 0 && se !== this.getEdge(SOUTH))
        return false;
    if (we !== 0 && we !== this.getEdge(WEST))
        return false;
    return true;
};

Card.prototype.test = function () {
    var ne = getType(this.x, this.y - 1, SOUTH);
    var ee = getType(this.x + 1, this.y, WEST);
    var se = getType(this.x, this.y + 1, NORTH);
    var we = getType(this.x - 1, this.y, EAST);

    if (ne !== 0 && ne !== this.getEdge(NORTH))
        return false;
    if (ee !== 0 && ee !== this.getEdge(EAST))
        return false;
    if (se !== 0 && se !== this.getEdge(SOUTH))
        return false;
    if (we !== 0 && we !== this.getEdge(WEST))
        return false;
    return true;
};
